<?php 

	require "templates/header.php";

	if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");
}

require "config/dbconfig.php";

if (isset($_POST['task-type'])) {
	include 'utils/remove.php';
}

?>


			<!-- Main -->
			<section class="wrapper style1">
				<div class="container">

					<!-- My Tasks List-->
					<section>
						<article>
							<header>
								<h2>
									<u><i class="fa fa-list-ol" style="font-size:36px;"></i>__My To Do List</u>
								</h2>
								<h4/>
							</header>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th></th>
											<th>Type</th>
											<th>Title</th>
											<th>Remove</th>
											<th>Deadline</th>
											<th>Word Count</th>
										</tr>
									</thead>
									<tbody>												
										
											<?php  
												$userid = ($_SESSION["user_id"]);
					              $stmt = $db_con->prepare("SELECT * from tasks t
					              	JOIN categories c on c.category_id = t.category_id
					              	WHERE task_id in(
					              		SELECT task_id from
					              		task_taskstatus WHERE
					              		status_id = 2)
					              	AND task_id in (
					              			SELECT ctd.task_id from claimedtask_details ctd
					              			JOIN claimedtasks ct on ct.claimed_id = ctd.claimed_id
					              			WHERE student_id =".$userid." 
					              		)");

					              $stmt->execute(array($userid));
					              //$stmt->debugDumpParams();
					              
					              $id = 0;
					              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					                $id = $id+1;
					                $taskid = $row['task_id'];
					                $title = $row['title'];
					                $category = $row['category'];
					                $deadline = date('d-m-Y',strtotime($row['expiry_date']));
					                $wordcount = $row['word_count'];
					                printf('<tr>
					                				<th>%s</th>
																	<td>%s</td>
																	<td><a href="claimtask.php?%s">"%s"</a></td>
																	<td>',$id, $category, $taskid, $title);
					                include('templates/remove_form.php');
					                printf('</td>
																	<td>%s</td>
																	<td>%s</td>
																	</tr>', $deadline, $wordcount);
					              }


					            ?>

										</tr>
										

										</tbody>

									</table>
								</div>
							</article>
						
					<br>
					<br>
					
					
					
					<article>
							<header>
								<h2>
									<u><i class="fa fa-upload" style="font-size:36px;"></i>__My Uploads</u>
								</h2>
								<h4/>
							</header>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th></th>
											<th>Type</th>
											<th>Title</th>
											<th>Submitted</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>						

									
										

										<?php  
					              $stmt = $db_con->query("SELECT * from tasks t
					              	JOIN categories c on c.category_id = t.category_id
					              	JOIN task_taskstatus tts on tts.task_id = t.task_id
					              	JOIN taskstatuses ts on ts.status_id = tts.status_id
					              	WHERE creator_id = ".$userid);

					              $id = 0;
					              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					                $id = $id+1;
					                $taskid = $row['task_id'];
					                $title = $row['title'];
					                $category = $row['category'];
					                $submitted = date('d-m-Y',strtotime($row['created_date']));				
					                $status = $row['status'];
									
					                printf('<tr>
					                				<th>%s</th>
																	<td>%s</td>
																	<td><a href="claimtask.php?%s">"%s"</a></td>
																	<td>%s</td>
																	<td>%s</td>
																	</tr>'
														, $id, $category, $taskid, $title, $submitted, $status);
					                
					              }

					            ?>
										

										</tbody>

									</table>
								</div>
							</article>
						</div>
				</section>
			</section>





		</div>
		
		<!-- Footer -->
		<?php 

	require "templates/footer.php";

	if (!isset ($_SESSION)) {
  session_start();
}
?>

		

	</body>
</html>
