<?php

require "templates/header.php";

if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");
}
  
require "config/dbconfig.php";
	
	$userid = $_SESSION["user_id"];

  $stmt = $db_con->query("SELECT * from tasks t
  	JOIN categories c on c.category_id = t.category_id
  	JOIN task_taskstatus tts on tts.task_id = t.task_id
  	JOIN taskstatuses ts on ts.status_id = tts.status_id
  	WHERE creator_id = ".$userid);

	$submittedTaskCount = $stmt->rowCount();

	$stmt = $db_con->query("SELECT * from claimedtasks ct
		JOIN claimedtask_details ctd on ct.claimed_id = ctd.claimed_id
		JOIN task_taskstatus tts on tts.task_id = ct.task_id
		WHERE tts.status_id = 4
		AND ct.student_id =".$userid);

	$completedTaskCount = $stmt->rowCount();

	$stmt = $db_con->query("SELECT * FROM users
		WHERE users.user_id = ".$userid);

	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$userReputation = $row["reputation"];

	$stmt = $db_con->query("SELECT * from tasks t
		JOIN categories c on c.category_id = t.category_id
		  WHERE task_id in(
  		SELECT task_id from
  		task_taskstatus WHERE
  		status_id = 2)
  		AND task_id in (
  			SELECT ctd.task_id from claimedtask_details ctd
  			JOIN claimedtasks ct on ct.claimed_id = ctd.claimed_id
  			WHERE student_id =".$userid." 
  		)
  	AND expiry_date > now()");

	$tasksToDo = $stmt->rowCount();

?>

			<!-- Main -->
			<section class="wrapper style1">
				<div class="container">
					<div class="row 200%">
						<div class="4u 12u(narrower)">
							<div id="sidebar">

								<!-- Sidebar -->

								<section>
									<h3>New to UL Document Review?</h3>
									<p>We provide a platform to students and university staff to facilitate the proofreading of theses, dissertations, assignments, and research papers by allowing users to upload proof reading tasks and to claim previously uploaded tasks.</p>
									<footer>
										<a href="help.php" class="button">Take Tour</a>
									</footer>
								</section>

								<section>
									<h3>Quicklinks</h3>
									<ul class="links">
										<li>
											<a href="create.php">Create new task</a>
										</li>
										<li>
											<a href="mytasks.php">View my tasks</a>
										</li>
										<li>
											<a href="#"/>
										</li>
										<li>
											<a href="search.php">Search tasks</a>
										</li>

									</ul>

								</section>

							</div>
						</div>
						<div class="8u  12u(narrower) important(narrower)">
							<div id="content">

								<!-- Content -->

								<article>
									<header>
										<h2>Dashboard</h2>
										<p>Welcome back <?php printf($_SESSION['username']); ?>!</p>
									</header>
									
									<div class="wrap">
    <div class="floatleft">
	<p><i class="fa fa-file-text-o" style="font-size:36px;"></i>.	Submitted Tasks: <strong><?php echo $submittedTaskCount ?></strong></p>
	<p><i class="fa fa-list-ol" style="font-size:36px;"></i>.	Tasks to do: <strong><?php echo $tasksToDo ?></strong></p>
	</div>
    <div class="floatright">
	<p><i class="fa fa-check-square-o" style="font-size:36px;"></i>.	Completed Tasks: <strong><?php echo $completedTaskCount ?></strong></p>
	<p><i class="fa fa-smile-o" style="font-size:36px"></i>.	My Reputation: <strong><?php echo $userReputation ?></strong></p>
	</div>
    <div style="clear: both;">
		</div>
		
		</div>


									<p>The UL Document review site could provide and easy way for academics to submit and review proof reading tasks by allowing users to submit their own proof reading tasks as well as search for/complete tasks submitted by other users. Users can also provide feedback on the site regarding the quality of work done on a particular task which will affect the reputation score of the user which completed that task (This will encourage good quality work rewarding those who complete tasks satisfactorily as well as disincentivizing those who do not do good work).</p>

									<h3>Our Mission Statement</h3>
									<p>Currently if a university community member/academic needs proof reading on a document completed they must physically find someone willing to proof read the document. Often times the proof reader will be a friend/acquaintance of the academic and not necessarily a subject matter expert, else it can be a person well known to be good at the subject (A lecturer or a student who actively participates in class) who may be inundated with requests to look over papers. Another issue is that once the document has been given to the proof reader there are no repercussions to them doing a poor, unsatisfactory job and conversely there is no reward for doing a good job.</p>

									<p></p>
								</article>

							</div>
						</div>
					</div>
				</div>
			</section>


<!-- Footer -->
		<?php 

	require "templates/footer.php";


?>

		</div>



	</body>
</html>