
<?php  

require "templates/header.php";

              if (!isset ($_SESSION)) {
                session_start();
                
              } 

              if (isset($_SESSION["user_id"]) && $_SESSION["user_id"] != ''){ 
                //if user logged in then run a function to go to the home page
                header("Location:./home.php");
              }

?>

      <!-- Main -->
      <section class="wrapper style1">
        <div class="container">
            <div id="login">   
          <h1>Welcome Back!</h1>  

            <?php 
              if (isset($_POST["e"]) && isset($_POST["p"]) && trim($_POST["e"]) !='' && trim($_POST["p"]) != ''  ){
                  try {

                      require "config/dbconfig.php";
                      //$dbh = new PDO("mysql:host=localhost;dbname=proofreading", "root", "");
                      $dbh = $db_con;
                      $email = trim(strtolower($_POST["e"]));
                      $password = $_POST["p"];  
                      $passwordHash = "";
                
                      $stmt = $dbh->prepare("SELECT user_id, first_name, email, password, reputation FROM Users WHERE email = ?");
                      $stmt->execute(array($email));
                      $id = null;
                      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {        
                          $id = $row['user_id'];
                          $passwordHash = $row['password'];
                          $username = $row['first_name'];
                          $reputation = $row['reputation'];
                  }
              
                $siteSalt  = "ulbuynsell";
                $saltedHash = hash('sha256', $password.$siteSalt);
                
                if ($passwordHash == $saltedHash && !is_null($id)&& $reputation > -5) {
                  $_SESSION['user_id'] = $id; 
                  $_SESSION['username'] = $username; 
                  header("Location:./index.php");
                } else {
                  if ($reputation < 0) {
                    printf("<h2> You have been banned for violating the terms of use, please contact sysadmin@uldocumentwebsite.ie if you believe this is in error. </h2>");
                  }else{
                  printf("<h2> Password incorrect or account not found. </h2>");
                  }
                }

                } catch (PDOException $exception) {
                    printf("Connection error: %s", $exception->getMessage());
              
                }

              }
          ?>
          
          <form method="post">
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" name="e" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" name="p" required autocomplete="off"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button type="submit" class="button button-block"/>Log In</button>
          
          </form>
      
        </div>
      </section>

                


                <!-- Footer -->
		<?php 

	require "templates/footer.php";

?>
                </div>

              </div>

              

            </body>
          </html>
          