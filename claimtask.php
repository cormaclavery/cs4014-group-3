<?php 

	require "templates/header.php";

	if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");

  	
}

if (isset($_POST["taskid"])) {
  		
			//if a claim task use the claim.php 
  		if ($_POST["task-type"]=="claim") {
  			require "utils/claim.php";
  		}
  		//if a rate task use the rate.php
  		if ($_POST["task-type"]=="rate") {
  			require "utils/rate.php";
  		}

  		//if flagging task include flag.php
  		if ($_POST["task-type"]=="flag") {
  			require "utils/flag.php";
  		}
  		if ($_POST['task-type']=="cancel") {
  			require "utils/cancel.php";					# code...
  		}				
  	}



$error = "";

//if query string received then do a search for the task details
if($_SERVER["QUERY_STRING"] > 0){
	require "config/dbconfig.php";

	//get email of user to send work to
	$stmt = $db_con->prepare("SELECT * FROM claimedtask_details ctd
		JOIN claimedtasks ct on ctd.claimed_id = ct.claimed_id
		JOIN users u on u.user_id = ct.student_id
		WHERE ctd.task_id = ?");
  $stmt->execute(array($_SERVER["QUERY_STRING"]));
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  $useremail = $row['email'];

	//get user reputation

	$stmt = $db_con->prepare("SELECT * 
	FROM users u
	WHERE u.user_id = ?");
	$stmt->execute(array($_SESSION["user_id"]));

	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$reputation = $row['reputation'];

	//if reputation greater than 30 retrieve task flags
	$flagArray = array();
	if ($reputation >= 30) {
		$stmt = $db_con->prepare("SELECT flag_comment 
		FROM flags
		WHERE task_id = ?");
		$stmt->execute(array($_SERVER["QUERY_STRING"]));

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			array_push($flagArray, $row['flag_comment']);
		}
	}

	//get tags
	$stmt = $db_con->prepare("SELECT tag 
	FROM task_tag_details ttd
	JOIN tags t on t.tag_id = ttd.tag_id
	WHERE ttd.task_id = ?");
	$stmt->execute(array($_SERVER["QUERY_STRING"]));

	$tagArray = array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		array_push($tagArray, $row['tag']);
	}

	$stmt = $db_con->prepare("SELECT * FROM tasks t
		JOIN users u on u.user_id = t.creator_id
		JOIN categories c on c.category_id = t.category_id
		JOIN subjectstream s on s.subject_id = t.subject_id
		JOIN task_taskstatus tts on tts.task_id = t.task_id
		WHERE t.task_id = ?");
  $stmt->execute(array($_SERVER["QUERY_STRING"]));
  $rowCount = $stmt->rowCount();
  if ($rowCount != 1) {
  	printf("invalid task id please return to <a href='home.php'>home</a>");
  }else{
  	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	}





}


?>

			<section class="wrapper style1">
				<div class="container">
					<div class="row 200%">
						<div class="8u 12u(narrower)">
							<div id="content">

								<!-- Content -->

								<article>
									<header>
										<h2>
											<u>Task Preview</u>
										</h2>
										<p>Short Description:</p>
									</header>


									<p>
										<em>
											<?php 

											printf($row["description"]);

											?>

										</em>
									</p>

									
									
									<?php
										printf("<p><a href='documents/%s.pdf' target='_blank'>Sample</a></p>", $row["task_id"]);
									?>

									<p></p>
									
									<?php
										//if task belongs to the user do not give option to claim task instead give option to rate feedback

										if ($_SESSION['user_id']!=$row['creator_id']) {
											if($row['status_id']==1){
												include('templates/user_claimtask_view.php');
											}
											if($row['status_id']==2){
												printf("<p>Please email completed work to %s</p>", $row['email']);
											}
										}
										else if ($_SESSION['user_id']==$row['creator_id']&&$row['status_id']==2) {
											
											include('templates/user_ratetask_view.php');
										}
										if ($_SESSION['user_id']==$row['creator_id']&&$row['status_id']!=4) {
											include('templates/user_removetask_view.php');
										}
										if ($reputation >= 30) {
											include('templates/mod_view.php');
										}

									?>
									
								</article>

							</div>
						</div>
						<div class="4u 12u(narrower)">
							<div id="sidebar">

								<!-- Sidebar -->

								<section>
									<h3>Title:</h3>
									<p>
										<?php
											print($row["title"]);
										?>


									</p>

									<p>
										<strong>Submitted by: </strong>
										<?php

											printf($row["first_name"]." ".$row["last_name"]);
										?>
										</p>
									<p>
										<strong>Deadline Date </strong>
											<?php
												$timestamp = strtotime($row['expiry_date']);
												printf(date('d-m-Y',$timestamp));
											?>
										</p>
									<p>
										<strong>Task Type: </strong>
										<?php
											printf($row["category"]);
										?>
										</p>
									<p>
										<strong>Category: </strong>
										<?php
											printf($row["subject_stream"]);
										?>
										</p>
									<p>
										<strong>Tags:<br></strong>
										<?php
											foreach ($tagArray as $key => $value) {
												printf("%s<br>",$value);
											}
										?>
										</p>
									<p>
										<strong>Word Count: </strong>
										<?php
											printf($row["word_count"]);
										?>
									</p>
									<p>
										<strong>Page Count: </strong>6</p>

								</section>


							</section>

						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Footer -->
		<?php 

	require "templates/footer.php";

?>

	</div>


</body>
</html>
