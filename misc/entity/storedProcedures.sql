DROP PROCEDURE IF EXISTS `getUser`;
DELIMITER //
CREATE PROCEDURE `getUser`(IN `user_id` INT, IN `email` VARCHAR(128))
    READS SQL DATA
BEGIN
	if user_id='' then set id=null;end if;
	if email='' then set email=null;end if;
	
	select u.user_id, u.email, u.`first_name`, u.`last_name`, u.password, u.subject_id, u.reputation  
        from users u  
        where   (user_id is null or u.user_id = id)
            and (email is null or (LOWER(u.email) = LOWER(email)));

END//
DELIMITER ;



DROP PROCEDURE IF EXISTS `getTask`;
DELIMITER //
CREATE PROCEDURE `getTask`(IN `task_id` INT)
    READS SQL DATA
BEGIN
	if id='' then set id=null;end if;
	
	select t.`task_id`,t.`creator_id`,t.`title`,t.`description`,t.`created_date`,t.`expiry_date`, t.'word_count', t.'category',t.'file_id' 
		from items tasks 
		where   (task_id is null or t.task_id = id);

END//
DELIMITER ;


DROP PROCEDURE IF EXISTS `getAvailableTasks`;
DELIMITER //
CREATE PROCEDURE `getAvailableTasks`()
    READS SQL DATA
BEGIN
	
	select t.`task_id`,t.`creator_id`,t.`title`,t.`description`,t.`created_date`,t.`expiry_date`, t.'page_count', t.'category', t.'file_id' 
		from tasks t 
		where date(t.`expiry_date`) >= curdate() 
			and t.`id` not in (select si.`item_id` from solditems si)
			order by t.`created_date` desc;


END//
DELIMITER ;


DROP PROCEDURE IF EXISTS `addUser`;
DELIMITER //
CREATE PROCEDURE `addUser`(IN `email` VARCHAR(128), IN `first_name` VARCHAR(128), IN `last_name` VARCHAR(128), IN `password` CHAR(128), IN 'subject_id' INT(6), IN 'reputation' INT(2))
    READS SQL DATA
BEGIN
	INSERT INTO `users` (`user_id`, `email`, `first_name`, `last_name`, `password`, 'subject_id', 'reputation') VALUES (NULL, `email`, `first_name`, `last_name`, `password`, 'subject_id', 'reputation');
	call getUser(LAST_INSERT_ID(),'');

END//
DELIMITER ;


DROP PROCEDURE IF EXISTS `addTask`;
DELIMITER //
CREATE PROCEDURE `addTask`(IN `creator_id` INT, IN `title` VARCHAR(128), IN `description` VARCHAR(4096), IN 'created_date' DATETIME, IN 'expiry_date' DATETIME, IN 'file_id' INT(10), IN 'page_count' INT(11))
    READS SQL DATA
BEGIN
	INSERT INTO `tasks` (`task_id`, `creator_id`, `title`, `description`, `created_date`, `expiry_date`, 'file_id', 'page_count') VALUES (NULL, `creator_id`, `title`, `description` , NOW(), DATE_ADD(NOW(), INTERVAL 30 DAY), 'file_id', 'page_count');
	call getItem(LAST_INSERT_ID());
END//
DELIMITER ;


DROP PROCEDURE IF EXISTS `markTaskAsClaimed`;
DELIMITER //
CREATE PROCEDURE `markTaskAsClaimed`(IN `task_id` INT, IN `user_id` INT)
    READS SQL DATA
BEGIN
	INSERT INTO `claimedtasks` (`claimed_id`, `task_id`, `creator_id`, `claimed_date`, 'expiry_date') VALUES (NULL, `task_id`, `creator_id`, NOW(), DATE_ADD(NOW(), INTERVAL 30 DAY));
	select 1 as "result";
END//
DELIMITER ;