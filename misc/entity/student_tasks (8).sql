-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2017 at 06:49 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(5) UNSIGNED NOT NULL,
  `category` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category`) VALUES
(2, 'Essay'),
(3, 'Final Year Project'),
(5, 'Project'),
(4, 'Thesis (Doctorate)'),
(1, 'Thesis (Masters)');

-- --------------------------------------------------------

--
-- Table structure for table `claimedtasks`
--

CREATE TABLE `claimedtasks` (
  `claimed_id` int(10) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `student_id` int(8) UNSIGNED NOT NULL,
  `claimed_date` datetime NOT NULL,
  `claimed_expiry` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fileuploads`
--

CREATE TABLE `fileuploads` (
  `file_id` int(10) UNSIGNED NOT NULL,
  `file_bin` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flags`
--

CREATE TABLE `flags` (
  `flag_id` int(10) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `flag_comment` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subjectstream`
--

CREATE TABLE `subjectstream` (
  `subject_id` int(6) NOT NULL,
  `subject_stream` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjectstream`
--

INSERT INTO `subjectstream` (`subject_id`, `subject_stream`) VALUES
(100001, 'Computer Science'),
(100002, 'Geography'),
(100003, 'Biomedical'),
(100004, 'Medicine'),
(100005, 'History'),
(100006, 'Equine Science'),
(100007, 'Marine Science'),
(100008, 'Engineering'),
(100009, 'Public Administration'),
(100010, 'Kemmy Business School');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(128) NOT NULL,
  `subject_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag`, `subject_id`) VALUES
(1001, 'Programming', 100001),
(1002, 'Coding', 100001),
(1003, 'PHP', 100001),
(1004, 'JAVA', 100001),
(1005, 'Cobol', 100001),
(1006, 'Web Infrastructure', 100001),
(1007, 'Database', 100001),
(1008, 'SQL', 100001),
(1009, 'Systems Information', 100001),
(1010, 'Testing', 100001),
(1011, 'Implementation', 100001),
(1012, 'Geology', 100002),
(1013, 'Rock Formation', 100002),
(1014, 'Rivers', 100002),
(1015, 'Oceans', 100002),
(1016, 'Geopolitics', 100002),
(1017, 'Humans', 100002),
(1018, 'Weather', 100002),
(1019, 'Robot Legs', 100003),
(1020, 'Mechanics', 100003),
(1021, 'Aortic Intervention', 100003),
(1022, 'Drug Resistance', 100003),
(1023, 'Nano technology', 100003),
(1024, 'Pancreatic Devices', 100003),
(1025, 'Broken Legs', 100006),
(1026, 'Anatomy', 100006),
(1027, 'Intervention', 100006),
(1028, 'Insemination', 100006),
(1029, 'Herd Health', 100006),
(1030, 'Neigh Health', 100006),
(1031, 'Mane maintenance', 100006),
(1032, 'Irish Fish', 100007),
(1033, 'Sharks', 100007),
(1034, 'Coral Reef', 100007),
(1035, 'Depopulation', 100007),
(1036, 'Water Safety', 100007),
(1037, 'Overfishing', 100007),
(1038, 'EU Quotas', 100007),
(1039, 'Wave power generation', 100007),
(1040, 'Physics', 100008),
(1041, 'Mathematics', 100008),
(1042, 'Applied Maths', 100008),
(1043, 'Construction', 100008),
(1044, 'Design', 100008),
(1045, 'Applied Physics', 100008),
(1046, 'Site Safety', 100008),
(1047, 'Instruments', 100008),
(1048, 'Politics', 100009),
(1049, 'Management', 100009),
(1050, 'Public Sector', 100009),
(1051, 'Irish Economy', 100009),
(1052, 'History of Ireland', 100009),
(1053, 'Public Service', 100009),
(1054, 'Health and Safety', 100009);

-- --------------------------------------------------------

--
-- Table structure for table `task-tag-details`
--

CREATE TABLE `task-tag-details` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `task-taskstatus`
--

CREATE TABLE `task-taskstatus` (
  `status_id` int(10) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` bigint(20) UNSIGNED NOT NULL,
  `creator_id` int(8) UNSIGNED NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `category_id` int(5) UNSIGNED NOT NULL,
  `description` varchar(4096) CHARACTER SET utf8 DEFAULT NULL,
  `subject_id` int(6) NOT NULL,
  `word_count` int(11) DEFAULT NULL,
  `page_count` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taskstatuses`
--

CREATE TABLE `taskstatuses` (
  `status_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taskstatuses`
--

INSERT INTO `taskstatuses` (`status_id`, `status`) VALUES
(1000000001, 'Active'),
(1000000002, 'Claimed'),
(1000000003, 'Expired'),
(1000000004, 'Complete'),
(1000000005, 'Removed'),
(1000000006, 'User_Cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(8) UNSIGNED NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `subject_id` int(6) NOT NULL,
  `password` char(128) CHARACTER SET utf8 NOT NULL,
  `reputation` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `uk_category` (`category`);

--
-- Indexes for table `claimedtasks`
--
ALTER TABLE `claimedtasks`
  ADD PRIMARY KEY (`claimed_id`),
  ADD UNIQUE KEY `uk_tasks` (`task_id`,`student_id`),
  ADD UNIQUE KEY `fk_claimed_task` (`task_id`),
  ADD KEY `fk_task_claimer` (`student_id`);

--
-- Indexes for table `fileuploads`
--
ALTER TABLE `fileuploads`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `flags`
--
ALTER TABLE `flags`
  ADD PRIMARY KEY (`flag_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `subjectstream`
--
ALTER TABLE `subjectstream`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `task-tag-details`
--
ALTER TABLE `task-tag-details`
  ADD PRIMARY KEY (`tag_id`,`task_id`),
  ADD KEY `pf_task-tag-details_tasks` (`task_id`);

--
-- Indexes for table `task-taskstatus`
--
ALTER TABLE `task-taskstatus`
  ADD PRIMARY KEY (`status_id`,`task_id`),
  ADD KEY `fk_task-taskstatus_tasks` (`task_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`),
  ADD UNIQUE KEY `uk_title_creator` (`title`,`creator_id`),
  ADD KEY `fk_tasks_students` (`creator_id`),
  ADD KEY `category` (`category_id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `subject_id` (`subject_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `taskstatuses`
--
ALTER TABLE `taskstatuses`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `uk_email` (`email`),
  ADD KEY `subject_id` (`subject_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `claimedtasks`
--
ALTER TABLE `claimedtasks`
  MODIFY `claimed_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fileuploads`
--
ALTER TABLE `fileuploads`
  MODIFY `file_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `flags`
--
ALTER TABLE `flags`
  MODIFY `flag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subjectstream`
--
ALTER TABLE `subjectstream`
  MODIFY `subject_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100011;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1055;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taskstatuses`
--
ALTER TABLE `taskstatuses`
  MODIFY `status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000000007;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `claimedtasks`
--
ALTER TABLE `claimedtasks`
  ADD CONSTRAINT `fk_claimedtasks_tasks` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_claimedtasks_users` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `flags`
--
ALTER TABLE `flags`
  ADD CONSTRAINT `fk_flags_tasks` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `fk_tags_subjectstream` FOREIGN KEY (`subject_id`) REFERENCES `subjectstream` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `task-tag-details`
--
ALTER TABLE `task-tag-details`
  ADD CONSTRAINT `pf_task-tag-details_tags` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pf_task-tag-details_tasks` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `task-taskstatus`
--
ALTER TABLE `task-taskstatus`
  ADD CONSTRAINT `fk_task-taskstatus_tasks` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`task_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_task-taskstatus_taskstatuses` FOREIGN KEY (`status_id`) REFERENCES `taskstatuses` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `fk_tasks_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tasks_fileuploads` FOREIGN KEY (`file_id`) REFERENCES `fileuploads` (`file_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tasks_students` FOREIGN KEY (`creator_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tasks_subjectstream` FOREIGN KEY (`subject_id`) REFERENCES `subjectstream` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_subjectstream` FOREIGN KEY (`subject_id`) REFERENCES `subjectstream` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
