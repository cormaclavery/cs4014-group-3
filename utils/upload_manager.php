
<?php
//source: http://www.tutorialrepublic.com/php-tutorial/php-file-upload.php
//source: http://php.net/manual/en/function.move-uploaded-file.php
//source: labs & tutorials

//validate that task can be uploaded
//validate that picture can be uploaded
//upload task
//upload sample

require "config/dbconfig.php";                   



if (isset($_POST) && count ($_POST) > 0 && (isset($_POST["title"])) && (isset($_SESSION["user_id"]))) {
        
        if (sizeof($_POST) < 8) {
            printf("Please fill all fields </br>");
        }
        elseif (sizeof($_POST["tags"])>4) {
            printf("You have too many tags");
        }
        elseif(isset($_FILES["photo"]["error"])){
            if($_FILES["photo"]["error"] > 0){
            printf("Please upload a PDF sample");
            } else{
            $allowed = array("application/pdf");
            $filename = $_FILES["photo"]["name"];
            $filetype = $_FILES["photo"]["type"];
            $filesize = $_FILES["photo"]["size"];
            $tmp_name = $_FILES["photo"]["tmp_name"];
            $uploads_dir = $_SERVER['DOCUMENT_ROOT'] ."/UL-Document-Website/documents/";
            $name = basename($filename);

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $_FILES['photo']['tmp_name']);
            
        
            // Verify file size - 5MB maximum
            $maxsize = 5 * 1024 * 1024;
            if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
        
            // Verify MYME type of the file
            if(in_array($filetype, $allowed)){
                
                
                if(file_exists("upload/" . $_FILES["photo"]["name"])){
                    echo $_FILES["photo"]["name"] . " is already exists.";
                } else{
                  //we know that data has been posted that that the file should be able to be uplaoded sucessfully. Now we need to upload the task

                  
                  $ext = pathinfo($filename, PATHINFO_EXTENSION);

                    $tcreator = $_SESSION["user_id"];
                    $ttitle = $_POST["title"];
                    $tcatid = $_POST["category_id"];
                    $tdesc = $_POST["description"];
                    $tsubjectId = $_POST["subject_id"];
                    $twordcount = $_POST["word_count"]; 
                    $tpagecount = $_POST["page_count"];
                    $cdate = date("Y-m-d H:i:s", time());
                    $edate = date("Y-m-d H:i:s", time() + (7 * 24 * 60 * 60));
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);

                    //$stmt = $db_con->prepare("INSERT INTO `tasks` (`task_id`, `creator_id`, `title`, `category_id`, `description`, `subject_id`, `created_date`, `expiry_date`,`file_id`) VALUES (NULL, ':tcreator',':ttitle',':tcatid',':tdesc',':tsubjectId',':twordcount',':tpagecount',':cdate',':edate', 1);");


                    $stmt = $db_con->prepare("INSERT INTO tasks SET task_id = NULL, creator_id =  :tcreator, title = :ttitle, category_id = :tcatid, description = :tdesc, subject_id = :tsubjectId, word_count = :twordcount, page_count = :tpagecount, created_date = :cdate, expiry_date = :edate, file_id = 1"); 
                                                  
                    $stmt->execute(array(":tcreator" => $tcreator,                    
                    ":ttitle" => $ttitle,
                    ":tcatid" => $tcatid,
                    ":tdesc" => $tdesc,
                    ":tsubjectId" => $tsubjectId,
                    ":twordcount" => $twordcount,
                    ":tpagecount" => $tpagecount,
                    ":cdate" => $cdate,
                    ":edate" => $edate));

                    
                    $insertId = $db_con->lastInsertId();

                    $rowCount = $stmt->rowCount();

                      if($rowCount > 0){
                        move_uploaded_file($tmp_name, $uploads_dir.$insertId.".".$ext);

                        //set task status to active
                        $stmt = $db_con->prepare("INSERT INTO task_taskstatus SET status_id = 1, task_id =  :taskid"); 
                                                  
                        $stmt->execute(array(":taskid" => $insertId));

                        //add tags to the table
                        $tags = $_POST["tags"];
                        foreach ($tags as $key => $value) {
                            $stmt = $db_con->prepare("INSERT INTO task_tag_details SET tag_id = :tagid, task_id =  :taskid");
                            $stmt->execute(array(":taskid" => $insertId, ":tagid" => $value)); 
                        }


                        echo "Your file was uploaded successfully.";
                      }
                      else{
                        echo "Error: There was a problem uploading your file - please try again.";
                      }

                      
                      
                } 
            } else{
                echo "Error: There was a problem uploading your file - please try again."; 
            }
        }
    } else{
        echo "Error: Invalid parameters - please contact your server administrator.";
    }
}
    //commit after everything confirmed




?>