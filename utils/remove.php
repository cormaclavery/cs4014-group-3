<?php

  //scipt to remove a to do task, should reduce users reputation by 1

if (!isset ($_SESSION)) {
  session_start();
}


require "config/dbconfig.php";

$taskid = $_POST["taskid"];
$currentdate = date("Y-m-d H:i:s", time()); 

//first delete entry in the claimed task details view
$stmt = $db_con->prepare("DELETE FROM claimedtask_details WHERE `claimedtask_details`.`task_id` =".$taskid); 
  $stmt->execute();

//get own reputation and then reduce by 1

  $stmt = $db_con->prepare("SELECT reputation FROM users 
    WHERE user_id = ?");
  $stmt->execute(array($_SESSION['user_id']));

  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  $newrep = $row['reputation'] - 1;

  //update users reputation

  $stmt = $db_con->prepare("UPDATE `users` SET `reputation` = ? WHERE `users`.`user_id` =".$_SESSION['user_id']); 
  $stmt->execute(array($newrep));

  //change task status based on time
  $stmt = $db_con->prepare("SELECT * FROM tasks 
    WHERE task_id = ?");
  $stmt->execute(array($taskid));
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($row['expiry_date'] < $currentdate) {
    $newStatus = '3';
  }else{
    $newStatus = '1';
  }

  $stmt = $db_con->prepare("UPDATE `task_taskstatus` SET `status_id` = ? WHERE `task_taskstatus`.`task_id` =".$taskid); 
  $stmt->execute(array($newStatus));


?>