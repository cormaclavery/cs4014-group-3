<?php

if (!isset ($_SESSION)) {
  session_start();
}


require "config/dbconfig.php";

$taskid = $_POST["taskid"];
$currentdate = date("Y-m-d H:i:s", time()); 

//script that will rate a task, mark it complete  and then change effected user score based on wheter it was returned on time and how rated

//first mark complete
$stmt = $db_con->prepare("UPDATE `task_taskstatus` SET `status_id` = '4' WHERE `task_taskstatus`.`task_id` =".$taskid); 
  $stmt->execute(); 

//get user who completed the task
  $stmt = $db_con->prepare("SELECT user_id, reputation FROM claimedtask_details ctd
    JOIN claimedtasks ct on ctd.claimed_id = ct.claimed_id
    JOIN users u on u.user_id = ct.student_id
    WHERE ctd.task_id = ?");
  $stmt->execute(array($taskid));

$row = $stmt->fetch(PDO::FETCH_ASSOC);
$rateduserid = $row['user_id'];
$newrep = $row['reputation'];

//calculate new reputation
$newrep = $newrep + $_POST['task_rating'];
if ($_POST['returned_on_time']=="false") {
  $newrep = $newrep -5;
}
//if different to old reputation then update the database

if ($newrep != $row['reputation']) {
  $stmt = $db_con->prepare("UPDATE `users` SET `reputation` = ? WHERE `users`.`user_id` =".$rateduserid); 
  $stmt->execute(array($newrep)); 
}



?>