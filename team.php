<?php 

	require "templates/header.php";

	if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");
}

require "config/dbconfig.php";

?>


<!-- Main -->
<section class="wrapper style1">
	<div class="container">

<div class="row 200%">
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<img src="images/cormac.png">
									<h3>Cormac</h3>
									<p>A driven, logical and creative individual with 4 years experience working as part of a team of Software developers, testers and IT specialists in the financial technology industry. Passionate about software development and new technologies.</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<img src="images/hugh.png">
									<h3>Hugh</h3>
									<p>High performing individual with strong influencing and analytical skills underpinned by diverse financial and project management experience in software services. Possessing strong initiative, drive and energy to achieve goals and results.</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<img src="images/connell.png">
									<h3>Connell</h3>
									<p>Higher Diploma Software Development student in the University of Limerick. Currently studying Java, SQL and HTML. Experience with Android App development, website creation and Raspberry Pi hardware projects.</p>
								</div>
							</section>
						</div>


	</div>
	</div>




	<!-- Footer -->
	<?php 

	require "templates/footer.php";


?>



</body>
</html>

