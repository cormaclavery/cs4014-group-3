<?php 

require "templates/header.php";

if (!isset ($_SESSION)) {
  session_start();
  
}

if (isset($_SESSION["user_id"]) && $_SESSION["user_id"] != ''){ 
  //if user logged in then run a function to go to the home page
  header("Location:./home.php");
}
  

?>

      <!-- Main -->
      <section class="wrapper style1">
        <div class="container">
            <div id="login"> 
          
          <h1>Sign Up</h1>

  <?php
    require "config/dbconfig.php";
    $dbh = $db_con;

    //validate that information has been posted and that the posted data has value first_name
    if (isset($_POST) && count ($_POST) > 0 && (isset($_POST["first_name"]))) {
        
        $email = trim(strtolower($_POST["email"]));
        $patterns = ["/[0-9]+@studentmail.ul.ie/","/[a-z0-9._%+-]+@ul.ie/"];

        //validate email and if incorretc print message and do nothing further
        if(!(bool)preg_match($patterns[0], $email) && !(bool)preg_match($patterns[1], $email)){
          printf("<h2>Please use a UL email account e.g. @studentmail.ul.ie or @ul.ie</h2>");
        }
        else{
        $firstName = htmlspecialchars(ucfirst(trim($_POST["first_name"]))); 
        $lastName = htmlspecialchars(ucfirst(trim($_POST["last_name"])));
        $subjectId = trim(strtolower($_POST["subject_id"]));
        $passOne = $_POST["pass_one"]; 
        $passTwo = $_POST["pass_two"];
        //check wheter user/email alerady exists
        
        //$dbh = new PDO("mysql:host=localhost;dbname=proofreading", "root", "");
        
        $stmt = $dbh->prepare("SELECT user_id, email, password FROM Users WHERE email = ?");
        $stmt->execute(array($email));
        $rowCount = $stmt->rowCount();
        if ($passOne != $passTwo) { //in case Javascript is disabled.
          printf("<h2> Passwords do not match. </h2>");
        } 
        else {
          if ($rowCount > 0) {
            printf("<h2> An account already exists with the given email.</h2>");
        } else {
          $query = "INSERT INTO users SET email = :email, first_name = :first_name, last_name = :last_name, password = :password, subject_id = :subject_id, reputation = 5" ;
          $stmt = $dbh->prepare($query);
          $siteSalt = "ulbuynsell";
          $saltedHash = hash('sha256', $passOne.$siteSalt);
          $affectedRows = $stmt->execute(array(':email' => $email, ':first_name' => $firstName, ':last_name' => $lastName, ':password' => $saltedHash, ':subject_id' => $subjectId));
        if ($affectedRows > 0) {
          $insertId = $dbh->lastInsertId();
          printf("<h2> Welcome %s! Please <a href=\"./login.php\"> login </a> to proceed. </h2>", $firstName);
          //logout first
          /*http://php.net/manual/en/function.session-unset.php*/
          session_unset(); session_destroy(); session_write_close(); setcookie(session_name(),'',0,'/');
          session_regenerate_id(true);
          } 
        }
      }
    } 
  } 

    //get list of subject_ids and subject names
        






  ?>
          
          <form method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" name="first_name" required="required" maxlength="60"/>
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text" name="last_name" maxlength="60"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              UL Email Address<span class="req">*</span>
            </label>
            <input type="email" name="email" required="required" maxlength="60"/>
          </div>
      
          <div class="field-wrap">
            <select name="subject_id" value="Subject stream" required="required">
            <option disabled selected>Select Department</option>
            <?php  
              $stmt = $dbh->query("SELECT * from subjectstream");
              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = $row['subject_id'];
                $title = $row['subject_stream'];
                printf("<option value='%s'>%s</option>", $id, $title);
              }

            ?>
        </select>
  
  </div>

          
          <div class="field-wrap">
            <label>
              Set Password<span class="req">*</span>
            </label>
            <input type="password" name="pass_one" required="required" maxlength="50"/>
          </div>
         
  <div class="field-wrap">
            <label>
              Confirm Password<span class="req">*</span>
            </label>
            <input type="password" name="pass_two" required="required" maxlength="50"/>
          </div>
          <p></p>
		  <p>By registering you agree to the Terms and Conditions</p>
          <button type="submit" class="button button-block"/>Get Started</button>
          
          </form>
      
        </div>
      </section>

                


                <!-- Footer -->
		<?php 

	require "templates/footer.php";

?>

              </div>

              
			  
			  

            </body>
          </html>
          