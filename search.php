<?php 

	require "templates/header.php";

	if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");
}

require "config/dbconfig.php";

?>


			<!-- Main -->
			<section class="wrapper style1">
				<div class="container">

					<!-- My Tasks List-->
					<section>
						<article>
							<header>
                        <h2>
                          <u>Spotlight</u>
                        </h2>
                        <h4>Showing all tasks related to your subject stream</h4>
                      </header>
							
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>Type</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Submitted<i class="fa fa-angle-down" style="font-size:26px;"></th>
                      <th>Deadline<i class="fa fa-angle-down" style="font-size:26px;"></th>
                      <th>Word Count</th>
										</tr>
									</thead>
									<tbody>												
										
											<?php  
												$userid = ($_SESSION["user_id"]);
											  $stmt = $db_con->prepare("SELECT * from tasks t
											    JOIN categories c on c.category_id = t.category_id
											    JOIN subjectstream s on s.subject_id = t.subject_id
											    WHERE task_id in(
											      SELECT task_id from
											      task_taskstatus WHERE
											      status_id = 1)
											    AND task_id not in (
											        SELECT task_id from claimedtask_details
											      )
											    AND expiry_date > now()
											    AND (
											    t.subject_id = (SELECT subject_id from users where user_id = :userid)
											    ||
											    t.task_id in (SELECT task_id from task_tag_details ttd
											    	JOIN tags t on t.tag_id = ttd.tag_id
											    	WHERE t.subject_id = (SELECT subject_id from users where user_id = :userid))
											    )
											    AND creator_id <> :userid");

											  $stmt->execute(array(':userid' => $userid));
											  //$stmt->debugDumpParams();
											  //printf($stmt->rowCount());
					              
					              
					             
					              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					                				$taskid = $row['task_id'];
                                  $title = $row['title'];
                                  $category = $row['category'];
                                  $submitted = date('d-m-Y',strtotime($row['created_date']));
                                  $subjectstream = $row['subject_stream']; 
                                  $deadline = date('d-m-Y',strtotime($row['expiry_date']));
                                  $wordcount = $row['word_count'];
					                
					                printf('<tr>
					                				<th>%s</th>
																	<td><a href="claimtask.php?%s">"%s"</a></td>
																	<td>%s</td>
																	<td>%s</td>
																	<td>%s</td>
																	<td>%s</td>
																	</tr>', $category, $taskid, $title, $subjectstream, $submitted, $deadline, $wordcount);
					              }


					            ?>

										</tr>
										

										</tbody>

									</table>
								</div>
							</article>

							<article>
							<header>
                        <h2>
                          <u>All tasks</u>
                        </h2>
                        <h4>Showing all unclaimed tasks</h4>
                      </header>
							
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>Type</th>
                      <th>Title</th>
                      <th>Category</th>
                      <th>Submitted<i class="fa fa-angle-down" style="font-size:26px;"></th>
                      <th>Deadline<i class="fa fa-angle-down" style="font-size:26px;"></th>
                      <th>Word Count</th>
										</tr>
									</thead>
									<tbody>												
										
											<?php  
												$userid = ($_SESSION["user_id"]);
											  $stmt = $db_con->prepare("SELECT * from tasks t
											    JOIN categories c on c.category_id = t.category_id
											    JOIN subjectstream s on s.subject_id = t.subject_id
											    WHERE task_id in(
											      SELECT task_id from
											      task_taskstatus WHERE
											      status_id = 1)
											    AND task_id not in (
											        SELECT task_id from claimedtask_details
											      )
											    AND expiry_date > now()
											    AND creator_id <>".$userid);

											  $stmt->execute(array($userid));
											  //$stmt->debugDumpParams();
											  //printf($stmt->rowCount());
					              
					              
					             
					              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					                				$taskid = $row['task_id'];
                                  $title = $row['title'];
                                  $category = $row['category'];
                                  $submitted = date('d-m-Y',strtotime($row['created_date']));
                                  $subjectstream = $row['subject_stream']; 
                                  $deadline = date('d-m-Y',strtotime($row['expiry_date']));
                                  $wordcount = $row['word_count'];
					                
					                printf('<tr>
					                				<th>%s</th>
																	<td><a href="claimtask.php?%s">"%s"</a></td>
																	<td>%s</td>
																	<td>%s</td>
																	<td>%s</td>
																	<td>%s</td>
																	</tr>', $category, $taskid, $title, $subjectstream, $submitted, $deadline, $wordcount);
					              }


					            ?>

										</tr>
										

										</tbody>

									</table>
								</div>
							</article>
						
					<br>
					<br>
					
					
					
					
						</div>
				</section>
			</section>





		</div>
		
		<!-- Footer -->
		<?php 

	require "templates/footer.php";


?>



	</body>
</html>
