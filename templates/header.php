<!DOCTYPE HTML>
<!--
  Arcana by HTML5 UP
  html5up.net | @ajlkn
  Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
  <head>
    <title>UL Document Review</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	<script src="js/modernizr.js"></script>


  </head>

  <body>
    <div id="page-wrapper">

      <!-- Header -->
      <div id="header">

        <!-- Logo -->
        <h1>
          <a href="home.php" id="logo">UL Document Review</a>
        </h1>

        <!-- Nav -->
        <?php

          $currentPage = end(explode("/", $_SERVER['REQUEST_URI']));

        
          $loggedInMenus=["home.php","create.php","mytasks.php","search.php","help.php","team.php","logout.php","claimtask.php"];
          $loggedInTitles=["Home","Create Task", "My Tasks","Search", "Help", "Team", "Logout"];
          $loggedOutMenus=["login.php","register.php"];
          $loggedOutTitles=["Login","Register"];

        
        printf("<nav id='nav'>");
        printf("<ul>");
        //if logged in then show the loggedInMenuTitles
        if (!in_array($currentPage, $loggedOutMenus)){ 
          for ($i=0; $i < count($loggedInTitles); $i++) 
          {
            if($loggedInMenus[$i] == $currentPage)
            {
              printf("<li class ='current'> <a href=\"%s\"> %s</a></li>", $loggedInMenus[$i], $loggedInTitles[$i]);
            }
            else
            {
              printf("<li> <a href=\"%s\"> %s</a></li>", $loggedInMenus[$i], $loggedInTitles[$i]);
            }
          }
        }else {
          for ($i=0; $i < count($loggedOutTitles); $i++) { 
            if($loggedOutMenus[$i] == $currentPage)
            {
              printf("<li class ='current'> <a href=\"%s\"> %s</a></li>", $loggedOutMenus[$i], $loggedOutTitles[$i]);
            }
            else
            {
              printf("<li> <a href=\"%s\"> %s</a></li>", $loggedOutMenus[$i], $loggedOutTitles[$i]);
            }
          } 
        }

        ?>

            
          </ul>
        </nav>

      </div>