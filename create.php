<?php 

require "templates/header.php";

if (!isset ($_SESSION)) {
  session_start();
}

if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == ''){ 
  	header("Location:./login.php");
}



?>

			<!-- Main -->
			<section class="wrapper style1">
				<div class="container">

					<!-- Content -->

					<!-- Create Task Step 1-->
					<section>
						<article>
              <p>
                <?php
                    require 'utils/upload_manager.php';
                  ?> 
              </p>
							<header>

                  
                <h2>
									<u>Step 1</u>
								</h2>
								<h4>
									Please enter the following information about your submission:
								</h4>

								<form method="post" enctype="multipart/form-data">
						        
						        
    						

									<div class="top-row">
										<div class="field-wrap">
											<label>
                Task Title<span class="req">*</span>
											</label>
											<input name="title" type="text" required autocomplete="off" />
										</div>

										<div class="field-wrap">
											<select name="category_id" value="Category" required="required">
                          <option disabled selected>Select Category</option>
                          <?php  
                            $stmt = $db_con->query("SELECT * FROM `categories` ORDER BY `category_id` ASC");
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                              $id = $row['category_id'];
                              $title = $row['category'];
                              printf("<option value='%s'>%s</option>", $id, $title);
                            }

                          ?>
                          </select>
										</div>
									</div>

									<div class="field-wrap">
										<label>
              Short description of Task<span class="req">*</span>
										</label>
										<input name="description" type="text"required autocomplete="off"/>
									</div>


										<div class="top-row">
											<div class="field-wrap">
												<label>
                Word Count<span class="req">*</span>
												</label>
												<input name="word_count" type="text" required autocomplete="off" />
											</div>



											<div class="field-wrap">
												<label>
                Page Count<span class="req">*</span>
												</label>
												<input name="page_count" type="text"required autocomplete="off"/>
											</div>
											</div>

                      <div class="field-wrap">
					  Please select 4 tags max
                        <select name="tags[]" id="inscompSelected" multiple="multiple">
                            <?php
                            $stmt = $db_con->query("SELECT * FROM `tags` ORDER BY `tag_id` ASC");
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                              $id = $row['tag_id'];
                              $title = $row['tag'];
                              printf("<option value='%s'>%s</option>", $id, $title);
                            }

                            
                            ?>
                          </select>
                      </div>


										

										<div class="field-wrap">
            <select name="subject_id" value="Subject stream" required="required">
            <option disabled selected>Select Department</option>
            <?php  
              $stmt = $db_con->query("SELECT * from subjectstream");
              while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $id = $row['subject_id'];
                $title = $row['subject_stream'];
                printf("<option value='%s'>%s</option>", $id, $title);
              }

            ?>
        </select>
  
  </div>


									
										

						        <input type="file" name="photo" id="fileSelect">
								<p></p>
								<p>By uploading you agree to the <a href="help.php#privacy" target="_blank">Terms and Conditions</a></p>
						        <input class="button" type="submit" name="submit" value="Upload">

									</form>
									</div>




								</section>
								
									</div>

								<!-- Footer -->
		<?php 

	require "templates/footer.php";

	
?>



						</body>
					</html>
					